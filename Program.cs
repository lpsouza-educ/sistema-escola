﻿using System;

namespace SistemaEscola
{
    class Program
    {
        static void Main(string[] args)
        {
            Aluno[] alunos = new Aluno[5];
            Turma[] turmas = new Turma[5];
            int alunoIdx = 0, turmaIdx = 0;
            bool emUso = true;
            while (emUso)
            {
                Console.WriteLine("");
                Console.WriteLine("Bem vindo ao sistema escolar!");
                Console.WriteLine("");
                Console.WriteLine("Escolha uma das opções do menu:");
                Console.WriteLine("");
                Console.WriteLine("1) Adicionar aluno");
                Console.WriteLine("2) Adicionar professor");
                Console.WriteLine("3) Adicionar turma");
                Console.WriteLine("4) Listar alunos");
                Console.WriteLine("5) Listar professores");
                Console.WriteLine("6) Listar turmas");
                Console.WriteLine("");
                Console.Write("Digite uma opção: ");
                string opcao = Console.ReadLine();

                if (int.Parse(opcao) == 1)
                {
                    if (alunoIdx < 5)
                    {
                        int id;
                        string nome, nascimento;
                        Console.Write("Digite um ID: ");
                        id = int.Parse(Console.ReadLine());
                        Console.Write("Digite o nome do Aluno: ");
                        nome = Console.ReadLine();
                        Console.Write("Digite a data de nascimento do Aluno: [DD/MM/AAAA] ");
                        nascimento = Console.ReadLine();
                        alunos[alunoIdx] = new Aluno(id, nome, nascimento);
                        alunoIdx++;
                    }
                    else
                    {
                        Console.WriteLine("Vetor cheio");
                    }
                }
                else if (int.Parse(opcao) == 2)
                {
                    // Adiciona professor
                }
                else if (int.Parse(opcao) == 3)
                {
                    if (turmaIdx < 5)
                    {
                        int id;
                        string nome, sala;
                        Console.Write("Digite um ID: ");
                        id = int.Parse(Console.ReadLine());
                        Console.Write("Digite o nome do Turma: ");
                        nome = Console.ReadLine();
                        Console.Write("Digite a sala do Turma: ");
                        sala = Console.ReadLine();
                        turmas[turmaIdx] = new Turma(id, nome, sala);
                        turmaIdx++;
                    }
                    else
                    {
                        Console.WriteLine("Vetor cheio");
                    }
                }
                else if (int.Parse(opcao) == 4)
                {
                    Console.WriteLine("ID\t|Nome do aluno\t|Nascimento");
                    for (int i = 0; i < alunos.Length; i++)
                    {
                        if (alunos[i] != null)
                        {
                            Console.WriteLine("{0}\t|{1}\t|{2}",
                                alunos[i].ObterId(),
                                alunos[i].ObterNome(),
                                alunos[i].ObterNascimento()
                            );
                        }
                    }
                }
                else if (int.Parse(opcao) == 5)
                {
                    // Listar professores
                }
                else if (int.Parse(opcao) == 6)
                {
                    Console.WriteLine("ID\t|Nome da sala\t|Sala");
                    for (int i = 0; i < turmas.Length; i++)
                    {
                        if (turmas[i] != null)
                        {
                            Console.WriteLine("{0}\t|{1}\t|{2}",
                                turmas[i].ObterId(),
                                turmas[i].ObterNome(),
                                turmas[i].ObterSala()
                            );
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Opção inválida!");
                }

                Console.Write("Continuar? (S/N) ");
                string escolha = Console.ReadLine();
                if (escolha.ToLower() == "n")
                {
                    emUso = false;
                }
                else if (escolha.ToLower() != "s")
                {
                    Console.WriteLine("Digite (s)im ou (n)ão.");
                }
            }
        }
    }
}
